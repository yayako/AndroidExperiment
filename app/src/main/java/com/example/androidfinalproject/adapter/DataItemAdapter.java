package com.example.androidfinalproject.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidfinalproject.R;
import com.example.androidfinalproject.activity.DetailInfoActivity;
import com.example.androidfinalproject.entity.DataItem;
import com.example.androidfinalproject.global.Global;
import com.example.androidfinalproject.utils.SerializeUtil;
import com.example.androidfinalproject.utils.StorageUtil;

import java.io.File;
import java.util.List;

public class DataItemAdapter extends RecyclerView.Adapter<DataItemAdapter.MyViewHolder> {
    private List<DataItem> dataItemList;
    private Context context;
    private File file;
    /** 0.for normal; 1.for collections; 2.for personal homepage */
    private int type;

    public DataItemAdapter(
            List<DataItem> dataItemList, Context applicationContext, File cacheDir, int type) {
        this.dataItemList = dataItemList;
        this.context = applicationContext;
        this.file = cacheDir;
        this.type = type;
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onBindViewHolder(
            @NonNull final DataItemAdapter.MyViewHolder holder, final int position) {
        DataItem dataItem = dataItemList.get(position);
        switch (type) {
            case 1:
                if (!dataItem.isCollected()) {
                    holder.setVisibility(1);
                    return;
                }
                break;
            case 2:
                if (!dataItem.getCreateBy().equals(Global.getCurUserName())) {
                    holder.setVisibility(1);
                    return;
                }
                break;
            default:
                break;
        }

        holder.collected.setImageResource(
                dataItem.isCollected()
                        ? R.drawable.ic_favorite_black_24dp
                        : R.drawable.ic_favorite_border_black_24dp);
        holder.title.setText(dataItem.getTitle());
        holder.detail.setText(dataItem.getDetails());
        holder.price.setText("￥" + String.format("%.2f", dataItem.getPrice()));
        StorageUtil.readImageFile(file, dataItem.getFileName(), holder.pic);
    }

    @NonNull
    @Override
    public DataItemAdapter.MyViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        View v =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.data_item_view, parent, false);
        final MyViewHolder vh = new MyViewHolder(v);
        vh.collected.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = vh.getAdapterPosition();
                        DataItem dataItem = dataItemList.get(position);
                        if (dataItem.isCollected()) {
                            ((ImageView) v)
                                    .setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            Toast.makeText(context, "取消喜欢成功！", Toast.LENGTH_SHORT).show();
                            dataItem.setCollected(false);
                            // 在收藏列表取消收藏后要设置其不可见
                            if (type == 1) {
                                vh.setVisibility(1);
                            }
                        } else {
                            ((ImageView) v).setImageResource(R.drawable.ic_favorite_black_24dp);
                            Toast.makeText(context, "喜欢成功！", Toast.LENGTH_SHORT).show();
                            dataItem.setCollected(true);
                        }
                    }
                });
        vh.itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = vh.getAdapterPosition();
                        DataItem dataItem = dataItemList.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putByteArray("buy", SerializeUtil.serialize(dataItem));
                        Intent intent = new Intent();
                        intent.putExtras(bundle);
                        intent.setClass(context, DetailInfoActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
        return vh;
    }

    @Override
    public int getItemCount() {
        return dataItemList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView detail;
        TextView price;
        ImageView collected;
        ImageView pic;
        Button bnt;

        MyViewHolder(@NonNull View v) {
            super(v);
            title = v.findViewById(R.id.title1);
            detail = v.findViewById(R.id.detail1);
            price = v.findViewById(R.id.price1);
            collected = v.findViewById(R.id.star);
            pic = v.findViewById(R.id.show);
            bnt = v.findViewById(R.id.button);
        }

        private void setVisibility(int visibility) {
            itemView.setVisibility(visibility);
            RecyclerView.LayoutParams params =
                    (RecyclerView.LayoutParams) itemView.getLayoutParams();
            if (visibility == View.VISIBLE) {
                params.width = RecyclerView.LayoutParams.MATCH_PARENT;
                params.height = RecyclerView.LayoutParams.WRAP_CONTENT;
            } else {
                params.width = 0;
                params.height = 0;
            }
            itemView.setLayoutParams(params);
        }
    }
}
