package com.example.androidfinalproject.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.androidfinalproject.R;
import com.example.androidfinalproject.entity.DataItem;
import com.example.androidfinalproject.utils.SerializeUtil;
import com.example.androidfinalproject.utils.StorageUtil;

public class DetailInfoActivity extends AppCompatActivity {
    private TextView title, detail, price, date;
    private ImageView pic;
    private DataItem dataItem;

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_info);

        title = findViewById(R.id.title3);
        detail = findViewById(R.id.detail3);
        date = findViewById(R.id.date3);
        price = findViewById(R.id.price3);
        pic = findViewById(R.id.imageView);

        Toolbar toolbar = findViewById(R.id.toolbar2);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

        Bundle bundle = getIntent().getExtras();
        dataItem = (DataItem) SerializeUtil.deserialize(bundle.getByteArray("buy"));
        title.setText(dataItem.getTitle());
        detail.setText(dataItem.getDetails());
        date.setText("买入时间：" + dataItem.getBoughtTime());
        price.setText("￥" + String.format("%.2f", dataItem.getPrice()));
        StorageUtil.readImageFile(getCacheDir(), dataItem.getFileName(), pic);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.right_toolbar_menu, menu);

        MenuItem item = menu.findItem(R.id.right_action);
        item.setTitle("联系卖家");
        item.setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO: 2020/6/2 联系卖家
                        Toast.makeText(
                                        com.example.androidfinalproject.activity.DetailInfoActivity
                                                .this,
                                        "联系卖家中...",
                                        Toast.LENGTH_SHORT)
                                .show();
                        if (ContextCompat.checkSelfPermission(
                                        com.example.androidfinalproject.activity.DetailInfoActivity
                                                .this,
                                        Manifest.permission.CALL_PHONE)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(
                                    com.example.androidfinalproject.activity.DetailInfoActivity
                                            .this,
                                    new String[] {Manifest.permission.CALL_PHONE},
                                    1);
                        } else {
                            call();
                        }
                        return true;
                    }
                });

        return super.onCreateOptionsMenu(menu);
    }

    private void call() {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel: " + dataItem.getPhone()));
            startActivity(intent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResult) {
        switch (requestCode) {
            case 1:
                if (grantResult.length > 0 && grantResult[0] == PackageManager.PERMISSION_GRANTED) {
                    call();
                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    View layout =
                            inflater.inflate(
                                    R.layout.custom_toast,
                                    (ViewGroup) findViewById(R.id.custom_toast_container));
                    ImageView imageView = layout.findViewById(R.id.imageView);
                    imageView.setImageResource(R.drawable.ic_do_not_disturb_black_24dp);
                    TextView textView = layout.findViewById(R.id.textView);
                    textView.setText("You denied the 'CALL_PHONE' permission");
                    Toast toast =
                            new Toast(
                                    com.example.androidfinalproject.activity.DetailInfoActivity
                                            .this);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                }
                break;
            default:
        }
    }
}
