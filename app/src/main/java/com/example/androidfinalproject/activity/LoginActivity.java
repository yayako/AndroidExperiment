package com.example.androidfinalproject.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.androidfinalproject.R;
import com.example.androidfinalproject.global.Global;

public class LoginActivity extends AppCompatActivity {
    EditText editText1, editText2;
    Button button;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText1 = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        button = findViewById(R.id.button);
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!editText2.getText().toString().equals("123456")) { // 密码错误
                            builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setTitle("密码错误！");
                            builder.setIcon(R.drawable.ic_error_black_24dp);
                            builder.setMessage(
                                    "Name: "
                                            + editText1.getText()
                                            + "\n"
                                            + "password: "
                                            + editText2.getText());
                            builder.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog, int which) { // ok的回调
                                            editText1.setText("");
                                            editText2.setText("");
                                        }
                                    });
                            builder.show();
                        } else { // 密码正确，跳转到浏览页面
                            Global.setCurUserName(editText1.getText().toString());
                            Intent intent = new Intent(LoginActivity.this, DataListActivity.class);
                            startActivity(intent);
                            Toast.makeText(LoginActivity.this, "登录成功！", Toast.LENGTH_SHORT).show();
                            editText1.setText(null);
                            editText2.setText(null);
                        }
                    }
                });
    }
}
