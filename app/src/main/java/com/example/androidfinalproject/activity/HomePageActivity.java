package com.example.androidfinalproject.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidfinalproject.R;
import com.example.androidfinalproject.adapter.DataItemAdapter;
import com.example.androidfinalproject.entity.DataItem;
import com.example.androidfinalproject.global.Global;

import java.util.Collections;
import java.util.List;

public class HomePageActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView textView;
    private TextView textView2;
    private List<DataItem> dataItemList;
    private RecyclerView recyclerView;
    private DataItemAdapter dataItemAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        dataItemList = Global.getDataItemList();
        textView = findViewById(R.id.username);
        textView.setText(Global.getCurUserName());
        textView2 = findViewById(R.id.myposts);
        textView2.setText("   我发布的");

        Toolbar toolbar = findViewById(R.id.toolbar3);
        toolbar.setTitle("个人主页");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

        recyclerView = findViewById(R.id.recyclerview3);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        // 添加Android自带的分割线
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        dataItemAdapter =
                new DataItemAdapter(dataItemList, getApplicationContext(), getCacheDir(), 2);
        recyclerView.setAdapter(dataItemAdapter);

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(
                        ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.START | ItemTouchHelper.END) {
                    @Override
                    public boolean onMove(
                            @NonNull RecyclerView recyclerView,
                            @NonNull RecyclerView.ViewHolder viewHolder,
                            @NonNull RecyclerView.ViewHolder target) {
                        int fromPosition = viewHolder.getAdapterPosition();
                        int toPosition = target.getAdapterPosition();
                        if (fromPosition < toPosition) {
                            for (int i = fromPosition; i < toPosition; i++) {
                                Collections.swap(dataItemList, i, i + 1);
                            }
                        } else {
                            for (int i = fromPosition; i > toPosition; i--) {
                                Collections.swap(dataItemList, i, i - 1);
                            }
                        }
                        dataItemAdapter.notifyItemMoved(fromPosition, toPosition);
                        return true;
                    }

                    @Override
                    public void onSwiped(
                            @NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        Toast.makeText(HomePageActivity.this, "删除成功！", Toast.LENGTH_SHORT).show();
                        dataItemList.remove(position);
                        dataItemAdapter.notifyItemRemoved(position);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }
}
