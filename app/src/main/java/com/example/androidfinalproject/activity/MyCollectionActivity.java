package com.example.androidfinalproject.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidfinalproject.R;
import com.example.androidfinalproject.adapter.DataItemAdapter;
import com.example.androidfinalproject.global.Global;

public class MyCollectionActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DataItemAdapter dataItemAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_collection);

        Toolbar toolbar = findViewById(R.id.toolbar5);
        toolbar.setTitle("我的收藏");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        // 返回的点击事件
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setResult(2);
                        finish();
                    }
                });

        recyclerView = findViewById(R.id.recyclerview2);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        // 添加Android自带的分割线
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        dataItemAdapter =
                new DataItemAdapter(
                        Global.getDataItemList(), getApplicationContext(), getCacheDir(), 1);
        recyclerView.setAdapter(dataItemAdapter);
    }
}
