package com.example.androidfinalproject.global;

import com.example.androidfinalproject.entity.DataItem;

import java.util.List;

/**
 * 全局变量
 *
 * <p>本来是通过将list序列化通过bundle传递于各activity之间，但是他们是全局共享的变量，而序列化传递的只是一份深拷贝，无法达到效果
 */
public class Global {
    private static List<DataItem> dataItemList;
    private static String curUserName;

    public static String getCurUserName() {
        return curUserName;
    }

    public static void setCurUserName(String curUserName) {
        Global.curUserName = curUserName;
    }

    public static List<DataItem> getDataItemList() {
        return dataItemList;
    }

    public static void setDataItemList(List<DataItem> dataItemList) {
        Global.dataItemList = dataItemList;
    }
}
