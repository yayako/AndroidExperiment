package com.example.androidfinalproject.entity;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class DataItem implements Serializable {

    private String createBy;
    private String title;
    private String details;
    private String fileName;
    private String boughtTime;
    private double price;
    private boolean isCollected;
    private String phone;

    public DataItem(
            @NotNull String createBy,
            @NotNull String title,
            String details,
            String fileName,
            String boughtTime,
            @NotNull double price,
            @NotNull String phone) {
        this.createBy = createBy;
        this.title = title;
        this.details = details == null ? "这个人很懒，什么都没有写。" : details;
        this.fileName = fileName;
        this.boughtTime = boughtTime;
        this.price = price;
        this.phone = phone;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBoughtTime() {
        return boughtTime;
    }

    public void setBoughtTime(String boughtTime) {
        this.boughtTime = boughtTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isCollected() {
        return isCollected;
    }

    public void setCollected(boolean collected) {
        isCollected = collected;
    }
}
